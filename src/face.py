import cv2
import numpy as np 
import base64

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

def detect(frame):
    img = np.frombuffer(frame, dtype='uint8')
    frame = cv2.imdecode(img, cv2.IMREAD_COLOR)
    gray = cv2.imdecode(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 22)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray, 1.3, 22)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (255, 0, 0), 4)
    buffer_img = cv2.imencode('.png',frame)[1].tostring()
    return base64.encodestring(buffer_img)
if __name__ == '__main__':
    with open(sys.argv[1], 'rb') as f:
        file = f.read()
    img = detect(file)
    cv2.imwrite('image.jpeg', img)

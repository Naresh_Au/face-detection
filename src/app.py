from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import base64


import face

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload', methods = ['GET', 'POST'])
def uploaded_file():
    if request.method == 'POST':
        f = request.files['file']
        img = f.read()
        img = face.detect(img)
    return render_template('index.html', img=str(img, 'utf-8'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)

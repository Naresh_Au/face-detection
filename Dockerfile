FROM python:3.7-slim as base

FROM base as builder
RUN mkdir /install
WORKDIR /install
COPY src/requirements.txt /requirements.txt
RUN pip install --target=/install -r /requirements.txt

FROM base
RUN apt-get update && \
  apt-get install -y --no-install-recommends \
  libsm6 \
  libxext6 \
  libxrender-dev \
  libglib2.0-0 \
  && rm -rf /var/lib/apt/lists/*
COPY --from=builder /install /usr/local/lib/python3.7/site-packages
COPY src /src
WORKDIR /src
EXPOSE 80
CMD ["python", "app.py"]

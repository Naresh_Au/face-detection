## How to use

### Use pre-built docker image

[Docker Hub](https://hub.docker.com/repository/docker/bishnoink/face-dection)

```sh
docker pull bishnoink/face-dection:latest
sudo docker run --rm -d -p 80:80 bishnoink/face-dection:latest
```

Open http://docker_host_url ##in the web browser


#### Running on local machine

Clone the project

```sh
git clone https://gitlab.com/Naresh_Au/face-detection.git
```


#### Make sure Python3.7 is installed

```sh
cd face-detection/
python3 -m venv venv        # sudo apt-get install python3-venv
source venv/bin/activate
pip install -r src/requirements.txt
cd src
sed -i 's/port=80/port=5000/g' app.py
python app.py
```

Open [http://localhost:5000](http://localhost:5000)


#### Build your own image and run on docker host

```sh
cd face-detection/
sudo docker build -t face-detection .
sudo docker run --rm -d -p 80:80 face-detection
sudo docker ps   #check if container is up
```
Open [http://dockerhost]http://dockerhost

#### Main tools:
[Python](https://www.python.org/)
[Flask](https://flask.palletsprojects.com/en/1.1.x/)
[OpenCV model](https://github.com/opencv/opencv/tree/master/data/haarcascades)

For detailed list please refer src/requirements.txt